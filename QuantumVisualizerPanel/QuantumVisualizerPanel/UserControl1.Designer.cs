﻿namespace QuantumVisualizerPanel
{
    partial class quantumPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paintBckWrk = new System.ComponentModel.BackgroundWorker();
            this.visualPnl = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.visualPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // paintBckWrk
            // 
            this.paintBckWrk.DoWork += new System.ComponentModel.DoWorkEventHandler(this.paintBckWrk_DoWork);
            // 
            // visualPnl
            // 
            this.visualPnl.AutoScroll = true;
            this.visualPnl.Controls.Add(this.label1);
            this.visualPnl.Location = new System.Drawing.Point(3, 3);
            this.visualPnl.Name = "visualPnl";
            this.visualPnl.Size = new System.Drawing.Size(418, 279);
            this.visualPnl.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(286, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // quantumPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.visualPnl);
            this.DoubleBuffered = true;
            this.Name = "quantumPanel";
            this.Size = new System.Drawing.Size(424, 285);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UserControl1_Paint);
            this.visualPnl.ResumeLayout(false);
            this.visualPnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker paintBckWrk;
        private System.Windows.Forms.Panel visualPnl;
        private System.Windows.Forms.Label label1;
    }
}
