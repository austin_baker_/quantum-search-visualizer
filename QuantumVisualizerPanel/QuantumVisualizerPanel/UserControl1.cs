﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace QuantumVisualizerPanel
{
    public partial class quantumPanel : UserControl
    {
        #region External Controls
        private int _scaleUnitSize = 120;
        private float hadamardConstant = (float)1 / (float)Math.Sqrt(2);
        private State _state;
        private Mathematician grover;
        private List<LogicGate> _operations;
        private List<long> data = new List<long>();
        public List<long> Data
        {
            get { return data; }
            set { data = value; }
        }
        public quantumPanel()
        {
            InitializeComponent();
            _state = new State();
            _state.ScaleUnitSize = _scaleUnitSize;
            _state.Location = new Point(50, 50);
            _operations = new List<LogicGate>();
            //Load the state and the logic gates
            int dataMax = 3;
            for (int i = 0; i <= dataMax; i++)
                data.Add(dataMax - i);
            LoadState(data);
            LogicGate hadamard = generateHadamardGate(_state);
            hadamard.ScaleUnitSize = _scaleUnitSize;
            hadamard.Location = new Point(50, 50);
            LogicGate inverter = generateInverter(_state);
            inverter.ScaleUnitSize = _scaleUnitSize;
            inverter.Location = new Point(50, 50);
            LogicGate oracle = generateOracle(_state, data, 1);
            oracle.ScaleUnitSize = _scaleUnitSize;
            oracle.Location = new Point(50, 50);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            _operations.Add(oracle);
            _operations.Add(hadamard);
            _operations.Add(inverter);
            _operations.Add(hadamard);
            //_operations.Add(oracle);
            //_operations.Add(hadamard);
            //_operations.Add(inverter);
            //_operations.Add(hadamard);

            grover = new Mathematician();
            grover.test = label1;
            grover.SetOperation(_state, _operations);
            //grover.MathObjects.Add(_state);
            grover.MathObjects.Add(oracle);
            grover.Workspace = visualPnl;
            while (grover.AlgorithmStep() != 1)
            {
                //nothing really
            }
        }
        private LogicGate generateDiffusionGate(State s)
        {
            float scaledHadamardConstant = (float)Math.Pow(hadamardConstant, s.StateVector.Count);
            LogicGate result = new LogicGate();

            for (int i = 0; i < 2 * (s.StateVector.Count); i++)
            {
                result.ValueMatrix.Add(new List<ComplexNumber>());
                for (int j = 0; j < 2 * (s.StateVector.Count); j++)
                {
                    result.ValueMatrix.ElementAt(i).Add(new ComplexNumber((float)Math.Pow(hadamardConstant, s.StateVector.Count), new DegreeAngle(-90 + 90 * (float)Math.Pow(-1d, (double)(i & j))), _scaleUnitSize, Point.Empty));
                }
            }

            return result;
        }
        
        private LogicGate generateHadamardGate(State s)
        {
            LogicGate result = new LogicGate();

            for (int i = 0; i < 2 * (s.StateVector.Count); i++)
            {
                result.ValueMatrix.Add(new List<ComplexNumber>());
                for (int j = 0; j < 2 * (s.StateVector.Count); j++)
                {
                    int rotationFactor = bitwiseDotProduct(i, j);
                    result.ValueMatrix.ElementAt(i).Add(new ComplexNumber((float)Math.Pow(hadamardConstant, s.StateVector.Count - 1), new DegreeAngle(-90 + 90 * (float)Math.Pow(-1d, rotationFactor)), _scaleUnitSize, new Point(50, 50)));
                }
            }

            return result;
        }
        private int bitwiseDotProduct(int lhs, int rhs)
        {
            int result = 0;

            while(lhs > 0 && rhs > 0)
            {
                result += lhs % 2 & rhs % 2;
                lhs = lhs >> 1;
                rhs = rhs >> 1;
            }

            return result;
        }
        private LogicGate generateInverter(State s)
        {
            LogicGate result = new LogicGate();

            for (int i = 0; i < 2 * (s.StateVector.Count); i++)
            {
                result.ValueMatrix.Add(new List<ComplexNumber>());
                for (int j = 0; j < 2 * (s.StateVector.Count); j++)
                {

                    if (i == j)
                    {
                        result.ValueMatrix.ElementAt(i).Add(ComplexNumber.One);
                    }
                    else
                    {
                        result.ValueMatrix.ElementAt(i).Add(ComplexNumber.Zero);
                    }
                    if (i == 0 && j == 0)
                    {

                        result.ValueMatrix.ElementAt(i).ElementAt(j).Angle.Angle = 180f;
                    }

                }
            }

            return result;
        }

        private LogicGate generateOracle(State s, List<long> data, long solution)
        {
            LogicGate result = new LogicGate();
            //label1.Text = data.IndexOf(solution).ToString();
            for (int i = 0; i < 2 * (s.StateVector.Count); i++)
            {
                result.ValueMatrix.Add(new List<ComplexNumber>());
                for (int j = 0; j < 2 * (s.StateVector.Count); j++)
                {

                    if (i == j)
                    {
                        result.ValueMatrix.ElementAt(i).Add(ComplexNumber.One);
                    }
                    else
                    {
                        result.ValueMatrix.ElementAt(i).Add(ComplexNumber.Zero);
                    }
                    if (i == (data.IndexOf(solution) * 2) + 1 && j == (data.IndexOf(solution)* 2) + 1)
                    {
                        label1.Text = i.ToString() + " " + j.ToString();
                        result.ValueMatrix.ElementAt(i).ElementAt(j).Angle.Angle = 180f;
                    }

                }
            }

            return result;
        }

        private Qubit bitToComplexMagnitude(int number, int bitNumber)
        {
            Qubit result = new Qubit();
            number = number >> bitNumber;
            number = number % 2;
            if (number == 1)
            {
                result.Off = ComplexNumber.One;
                result.On = ComplexNumber.One;
                result.On.Angle.Angle = 180f;
            }
            else
            {
                result.Off = ComplexNumber.One;
                result.Off.Angle.Angle = 180f;
                result.On = ComplexNumber.One;
            }
            return result;
        }
        private void UserControl1_Paint(object sender, PaintEventArgs e)
        {
            grover.Draw();
        }
        public void LoadState(List<long> input)
        {
            //Create enough qubits to hold the largest number in the list.
            //The largest number in the list is also the count of the list
            for (int qubits = 0; qubits < Math.Pow(2, countBinaryDigits(input.Count - 1)); qubits++)
            {
                if (qubits == 0)
                {
                    Qubit next = Qubit.Minus;
                    next.ScaleUnitSize = _scaleUnitSize;
                    next.Location = new Point(50, 50);
                    _state.StateVector.Add(next);
                }
                else
                {
                    Qubit next = new Qubit();
                    next.On = ComplexNumber.Zero;
                    next.Off = ComplexNumber.Zero;
                    _state.StateVector.Add(next);
                }
            }
        }
        private int countBinaryDigits(int count)
        {
            int result = 0;
            while (count > 0)
            {
                result++;
                count /= 2;
            }
            return result;
        }
        public int ScaleUnitSize
        {
            set { _scaleUnitSize = value; }
            get { return _scaleUnitSize; }
        }

        private void paintBckWrk_DoWork(object sender, DoWorkEventArgs e)
        {

        }
        #endregion
        public class DegreeAngle
        {
            private float _angle;
            public float Angle
            {
                set { _angle = value; }
                get { return _angle; }
            }
            public DegreeAngle(int angle)
            {
                this.Angle = (float)angle;
            }
            public DegreeAngle(float angle)
            {
                this.Angle = angle;
            }
            public void setFromRads(float angle)
            {
                this.Angle = (float)(angle / Math.PI * 180d);
            }
            public float AngleRads()
            {
                return (float)(this.Angle / 180f * Math.PI);
            }
            public static DegreeAngle operator -(DegreeAngle rhs)
            {
                return new DegreeAngle((float)-rhs.Angle);
            }
            public static DegreeAngle operator +(DegreeAngle lhs, DegreeAngle rhs)
            {
                return new DegreeAngle((float)lhs.Angle + (float)rhs.Angle);
            }
        }
        public class Trail : MathObject
        {
            private Point _head;
            private Point _tail;
            public Point Head
            {
                get { return this._head; }
                set { _head = value; }
            }
            public Point Tail
            {
                get { return this._tail; }
                set { _tail = value; }
            }
            public override void Draw(PaintEventArgs e)
            {
                Pen p = new Pen(Color.Black, 1.5f);
                e.Graphics.DrawLine(p, this.Head, this.Tail);
            }
        }
        public class LogicGate : MathObject
        {
            private List<List<ComplexNumber>> _valueMatrix;
            private bool _reScaled = false;
            public LogicGate()
            {
                _valueMatrix = new List<List<ComplexNumber>>();
            }
            public override void Draw(PaintEventArgs e)
            {
                if (!_reScaled)
                {
                    ReScale();
                }
                for (int row = 0; row < _valueMatrix.Count; row++)
                {
                    for (int column = 0; column < _valueMatrix.Count; column++)
                    {
                        _valueMatrix.ElementAt(row).ElementAt(column).Draw(e);
                    }
                }
            }
            public List<List<ComplexNumber>> ValueMatrix
            {
                get
                {
                    _reScaled = false;
                    return _valueMatrix;
                }
                set
                {
                    _reScaled = false;
                    _valueMatrix = value;
                }
            }
            private void ReScale()
            {
                int greatestRowLength = Int32.MinValue;
                for (int row = 0; row < _valueMatrix.Count; row++)
                {
                    if (_valueMatrix.ElementAt(row).Count > greatestRowLength)
                    {
                        greatestRowLength = _valueMatrix.ElementAt(row).Count;
                    }
                }
                float scaleFactor = (float)ScaleUnitSize / (float)greatestRowLength;
                for (int row = 0; row < _valueMatrix.Count; row++)
                {
                    for (int column = 0; column < _valueMatrix.ElementAt(row).Count; column++)
                    {
                        _valueMatrix.ElementAt(row).ElementAt(column).ScaleUnitSize = (int)scaleFactor;
                        _valueMatrix.ElementAt(row).ElementAt(column).Location = new Point(Location.X + (int)(column * scaleFactor * 100) / 50, Location.Y + (int)(row * scaleFactor * 100) / 50);
                    }
                }
                _reScaled = true;
            }
            public Vector transform(Vector input)
            {
                Vector result = new Vector();
                if (input.Elements.Count == _valueMatrix.Count)
                {
                    for (int row = 0; row < _valueMatrix.Count; row++)
                    {
                        ComplexNumber runningValue = ComplexNumber.Zero;
                        for (int column = 0; column < _valueMatrix.ElementAt(row).Count; column++)
                        {
                            ComplexNumber next = input.Elements.ElementAt(column) * _valueMatrix.ElementAt(row).ElementAt(column);
                            runningValue += next;
                        }
                        result.Elements.Add(runningValue);
                    }
                }
                return result;
            }
            public State transform(State input)
            {
                State result = new State();
                if (input.StateVector.Count == _valueMatrix.Count / 2)
                {
                    Vector simpleVector = new Vector();
                    for (int qubit = 0; qubit < input.StateVector.Count; qubit++)
                    {
                        simpleVector.Elements.Add(input.StateVector.ElementAt(qubit).On);
                        simpleVector.Elements.Add(input.StateVector.ElementAt(qubit).Off);
                    }
                    simpleVector = transform(simpleVector);
                    for (int element = 0; element < simpleVector.Elements.Count; element += 2)
                    {
                        Qubit next = new Qubit();
                        next.On = simpleVector.Elements.ElementAt(element);
                        next.Off = simpleVector.Elements.ElementAt(element + 1);
                        result.StateVector.Add(next);
                    }
                }
                return result;
            }
        }
        public class Vector : MathObject
        {
            private List<ComplexNumber> _elements;
            private bool _reScaled = false;
            public Vector()
            {
                _elements = new List<ComplexNumber>();
            }
            public List<ComplexNumber> Elements
            {
                get
                {
                    _reScaled = false;
                    return _elements;
                }
                set { _elements = value; }
            }
            public override void Draw(PaintEventArgs e)
            {
                if (!_reScaled)
                {
                    ReScale();
                }
                foreach (ComplexNumber element in Elements)
                {
                    element.Draw(e);
                }
            }
            private void ReScale()
            {
                float rescaleFactor = (float)ScaleUnitSize / (float)Elements.Count;
                for (int element = 0; element < Elements.Count; element++)
                {
                    Elements.ElementAt(element).ScaleUnitSize = (int)(rescaleFactor);
                    Elements.ElementAt(element).Location = new Point(Location.X, Location.Y + (int)(element * rescaleFactor * 100) / 50);
                }
                _reScaled = true;
            }
        }
        public class Qubit : MathObject
        {
            private ComplexNumber _on;
            private ComplexNumber _off;
            private bool _reScaled = false;
            public ComplexNumber On
            {
                set { _on = value; }
                get { return _on; }
            }
            public ComplexNumber Off
            {
                set { _off = value; }
                get { return _off; }
            }
            public override void Draw(PaintEventArgs e)
            {
                if (!_reScaled)
                {
                    ReScale();
                }
                On.Draw(e);
                Off.Draw(e);
            }
            private void ReScale()
            {
                On.Location = Location;
                Off.Location = new Point(Location.X, Location.Y + ScaleUnitSize);
                On.ScaleUnitSize = ScaleUnitSize / 2;
                Off.ScaleUnitSize = ScaleUnitSize / 2;
                _reScaled = true;
            }
            public int bitValue()
            {
                int result = 0;

                if (Off.Magnitude < On.Magnitude)
                {
                    result = 1;
                }
                return result;
            }
            public static Qubit Plus
            {
                get
                {
                    Qubit plus = new Qubit();
                    plus.On = new ComplexNumber(0, new DegreeAngle(0), 0, Point.Empty);
                    plus.Off = new ComplexNumber(1, new DegreeAngle(0), 0, Point.Empty);
                    return plus;
                }
            }
            public static Qubit Minus
            {
                get
                {
                    Qubit minus = new Qubit();
                    minus.Off = new ComplexNumber(0, new DegreeAngle(0), 0, Point.Empty);
                    minus.On = new ComplexNumber(1f, new DegreeAngle(0), 0, Point.Empty);
                    return minus;
                }
            }
        }
        public class State : MathObject
        {
            private List<Qubit> _stateVector;
            private bool _reScaled = false;
            public List<Qubit> StateVector
            {
                get
                {
                    _reScaled = false;
                    return _stateVector;
                }
                set { _stateVector = value; }
            }
            public State()
            {
                _stateVector = new List<Qubit>();
            }
            private void ReScale()
            {
                float rescaleFactor = (float)ScaleUnitSize / (float)_stateVector.Count;
                for (int qubit = 0; qubit < _stateVector.Count; qubit++)
                {
                    _stateVector.ElementAt(qubit).Location = new Point(Location.X, Location.Y + (int)(qubit * rescaleFactor * 100) / 50);
                    _stateVector.ElementAt(qubit).ScaleUnitSize = (int)rescaleFactor;
                }
            }
            public override void Draw(PaintEventArgs e)
            {
                if (!_reScaled)
                {
                    ReScale();
                }
                foreach (Qubit bit in _stateVector)
                {
                    bit.Draw(e);
                }
            }
            public int bitValue()
            {
                int result = 0;
                for (int i = 0; i <_stateVector.Count; i++)
                {
                    result += _stateVector.ElementAt(i).bitValue() << i;
                }
                return result;
            }
        }
        public class ComplexNumber : MathObject
        {
            static Random rgen;
            private float _magnitude;
            private float _magnitudeX;
            private float _magnitudeY;
            private DegreeAngle _angle;
            private Pen _linePen;
            private Pen _anglePen;
            public ComplexNumber()
            {
                if (ComplexNumber.rgen == null)
                    ComplexNumber.rgen = new Random();
            }
            public ComplexNumber(float magnitude, DegreeAngle angle, int scaleUnitSize, Point location)
            {
                if (ComplexNumber.rgen == null)
                    ComplexNumber.rgen = new Random();
                this.Magnitude = magnitude;
                this.Angle = angle;
                this.ScaleUnitSize = scaleUnitSize;
                this.Location = location;
            }
            public DegreeAngle Angle
            {
                set
                {
                    _angle = value;
                    calcXY();
                }
                get { return _angle; }
            }
            public float Magnitude
            {
                set
                {
                    _magnitude = value;
                    calcXY();
                }
                get { return _magnitude; }
            }
            public float MagnitudeX
            {
                get { return _magnitudeX; }
                set
                {   _magnitudeX = value;
                    calcMag();
                }
            }
            public float MagnitudeY
            {
                get { return _magnitudeY; }
                set
                {   
                    _magnitudeY = value;
                    calcMag();
                }
            }
            private void calcXY()
            {
                if (this.Angle != null)
                {
                    _magnitudeX = (float)(_magnitude * Math.Cos(_angle.AngleRads()));
                    _magnitudeY = (float)(_magnitude * Math.Sin(_angle.AngleRads()));
                }
            }
            private void calcMag()
            {
                _magnitude = (float)Math.Sqrt(Math.Pow((double)_magnitudeX, 2) + Math.Pow((double)_magnitudeY, 2));
                if (_angle == null)
                    _angle = new DegreeAngle(0);
                if (_magnitudeX != 0f)
                    this.Angle.setFromRads((float)Math.Atan(_magnitudeY / _magnitudeX));
                if (Math.Sign(_magnitudeX) == 1)
                {
                    if (Math.Sign(_magnitudeY) == 1)
                    {

                    }
                    else
                    {

                    }
                }
                else if (Math.Sign(_magnitudeX) == -1)
                {
                    if (Math.Sign(_magnitudeY) == 1 || Math.Sign(_magnitudeY) == 0)
                    {
                        this.Angle.Angle += 180f;
                    }
                    else
                    {

                    }
                }

            }
            public override void Draw(PaintEventArgs e)
            {
                if (_anglePen == null)
                    _anglePen = new Pen(Color.FromArgb(50, rgen.Next(255), rgen.Next(255), rgen.Next(255)), (int)(ScaleUnitSize * Magnitude));
                if (_linePen == null)
                    _linePen = new Pen(Color.FromArgb(0, 0, 0), (float)3);
                Point end = new Point(Location.X + (int)(MagnitudeX * ScaleUnitSize), Location.Y - (int)(MagnitudeY * ScaleUnitSize));
                e.Graphics.DrawLine(_linePen, Location, end);
                this.drawAngle(e);
            }
            private void drawAngle(PaintEventArgs e)
            {
                Point center = Location;
                center.X -= (int)(ScaleUnitSize * Math.Abs(Magnitude) / 2);
                center.Y -= (int)(ScaleUnitSize * Math.Abs(Magnitude) / 2);
                Rectangle r = new Rectangle(center, new Size((int)(ScaleUnitSize * Math.Abs(Magnitude)) + 1, (int)(ScaleUnitSize * Math.Abs(Magnitude)) + 1));
                if (this.Angle.Angle < 0)
                    this.Angle = new DegreeAngle(360 + this.Angle.Angle);
                e.Graphics.DrawArc(_anglePen, r, 0f, -1f * this.Angle.Angle);
            }
            public static ComplexNumber operator *(ComplexNumber lhs, ComplexNumber rhs)
            {
                ComplexNumber result = new ComplexNumber();
                result.Magnitude = lhs.Magnitude * rhs.Magnitude;
                result.Angle = lhs.Angle + rhs.Angle;
                return result;
            }
            public static ComplexNumber operator +(ComplexNumber lhs, ComplexNumber rhs)
            {
                ComplexNumber result = new ComplexNumber();
                //Calculating magnitude and angle:
                //Find x and y component of magnitude


                float xMag = lhs.MagnitudeX + rhs.MagnitudeX;
                float yMag = lhs.MagnitudeY + rhs.MagnitudeY;

                result.MagnitudeX = xMag;
                result.MagnitudeY = yMag;
                return result;
            }
            public static ComplexNumber One
            {
                get { return new ComplexNumber(1f, new DegreeAngle(0), 0, Point.Empty); }
            }
            public static ComplexNumber Zero
            {
                get { return new ComplexNumber(0f, new DegreeAngle(0), 0, Point.Empty); }
            }
        }
        public class MathObject
        {
            private Point _location;
            private int _scaleUnitSize;
            public Point Location
            {
                get { return _location; }
                set { _location = value; }
            }
            public int ScaleUnitSize
            {
                get { return _scaleUnitSize; }
                set { _scaleUnitSize = value; }
            }
            public virtual void Draw(PaintEventArgs e)
            {

            }
        }
        public class Mathematician : MathObject
        {
            private Control _workspace;
            private PaintEventArgs _e;
            private List<MathObject> _mathObjects;
            private List<LogicGate> _sequence;
            private State _data;
            public Label test;
            public void SetOperation(State data, List<LogicGate> sequence)
            {
                _data = data;
                _sequence = sequence;
            }
            public Mathematician()
            {
                this.MathObjects = new List<MathObject>();
                _sequence = new List<LogicGate>();
            }
            public Control Workspace
            {
                set
                {
                    _workspace = value;
                    _e = new PaintEventArgs(_workspace.CreateGraphics(), _workspace.ClientRectangle);
                }
                get { return _workspace; }
            }
            public List<MathObject> MathObjects
            {
                get { return _mathObjects; }
                set { _mathObjects = value; }
            }
            public void Draw()
            {
                foreach (MathObject mathObject in MathObjects)
                {
                    DrawObject(mathObject);
                }
            }
            public void DrawObject(MathObject mathObject)
            {
                mathObject.Draw(_e);
            }
            public void PassGate()
            {
                if (!(_sequence.Count == 0))
                {
                    State temp = _sequence.ElementAt(0).transform(_data);
                    temp.Location = _data.Location;
                    temp.ScaleUnitSize = _data.ScaleUnitSize;
                    _data = temp;
                    _sequence.RemoveAt(0);
                }
            }
            public int AlgorithmStep()
            {
                int result = 0;
                if (!(_sequence.Count == 0))
                {
                    PassGate();
                }
                else
                {
                    result = 1;
                }
                _mathObjects.Clear();
                _mathObjects.Add(_data);
                test.Text = (_data.bitValue()>>1).ToString();
                //Do some stuff to the objects' locations and such
                return result;
            }
        }

    }
}
