﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinearSearchVisualizer
{
    
    public partial class LinearVisualizerControl: UserControl
    {
        private int elements = 0;
        private bool _ready = false;
        private List<long> data = new List<long>();
	    linearVisualizer lv = new linearVisualizer();
        public bool Ready
        {
            get { return _ready; }
        }
        public List<long> Data
        {
            get { return data; }
            set { 
                data = value;
                Elements = data.Count();
            }
        }
        public LinearVisualizerControl()
        {
            InitializeComponent();
        }

        private void LinearVisualizerControl_Load(object sender, EventArgs e)
        {
        }
        public bool RunStep(long searchValue, ref int index)
        {
            searchNumberTxtBox.Text = searchValue.ToString();
            
            bool found = lv.visualizationStep(ref data, ref elementsLstBox, searchValue, ref index);
            if(found)
            {
                foundRchTxtBox.Text = "Found " + searchValue.ToString() + " after searching " + (index + 1).ToString() + " items.";
            }
            else if (index == -1)
            {
                foundRchTxtBox.Text = searchValue.ToString() + " was not found.";
            }
            else
            {
                foundRchTxtBox.Text = "Searching for:  " + searchValue.ToString() + "\nAt index:  " + index.ToString();
            }
            return found;
        }
        public void Clear()
        {
            elementsLstBox.Items.Clear();
        }
        public void Randomize()
        {
            if (!_ready)
                _ready = true;
        }
        public int Elements
        {
            get { return elements; }
            set { elements = value; }
        }
        public class linearVisualizer
        {
            private static Random rng = new Random();
            public linearVisualizer()
            {

            }
            public bool visualizationStep(ref List<long> data, ref ListBox elementsLstBox, long searchValue, ref int index)
            {
                if (index < data.Count)
                {
                    long currentitem = data[index];
                    
                    elementsLstBox.Items.Add(currentitem.ToString());
                    elementsLstBox.SelectedIndex = elementsLstBox.Items.Count - 1;
                    if (currentitem == searchValue)
                    {
                        return true;
                    }
                    index++;
                    return false;
                }
                else
                {
                    index = -1;
                    return false;
                }
                
            }
        }

        private void itemLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (long l in data)
            {
                
            }
        }

        private void itemLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            elementsLstBox.Items.Add("Hello");
            
        }

        private void UserControl1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
    
}

