﻿namespace LinearSearchVisualizer
{
    partial class LinearVisualizerControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchongForLbl = new System.Windows.Forms.Label();
            this.searchNumberTxtBox = new System.Windows.Forms.TextBox();
            this.foundRchTxtBox = new System.Windows.Forms.RichTextBox();
            this.elementsLstBox = new System.Windows.Forms.ListBox();
            this.itemLoader = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // searchongForLbl
            // 
            this.searchongForLbl.AutoSize = true;
            this.searchongForLbl.Location = new System.Drawing.Point(3, 6);
            this.searchongForLbl.Name = "searchongForLbl";
            this.searchongForLbl.Size = new System.Drawing.Size(76, 13);
            this.searchongForLbl.TabIndex = 0;
            this.searchongForLbl.Text = "Searching For:";
            // 
            // searchNumberTxtBox
            // 
            this.searchNumberTxtBox.Location = new System.Drawing.Point(85, 3);
            this.searchNumberTxtBox.Name = "searchNumberTxtBox";
            this.searchNumberTxtBox.ReadOnly = true;
            this.searchNumberTxtBox.Size = new System.Drawing.Size(332, 20);
            this.searchNumberTxtBox.TabIndex = 1;
            // 
            // foundRchTxtBox
            // 
            this.foundRchTxtBox.Location = new System.Drawing.Point(3, 348);
            this.foundRchTxtBox.Name = "foundRchTxtBox";
            this.foundRchTxtBox.ReadOnly = true;
            this.foundRchTxtBox.Size = new System.Drawing.Size(414, 96);
            this.foundRchTxtBox.TabIndex = 2;
            this.foundRchTxtBox.Text = "";
            this.foundRchTxtBox.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // elementsLstBox
            // 
            this.elementsLstBox.FormattingEnabled = true;
            this.elementsLstBox.Location = new System.Drawing.Point(6, 29);
            this.elementsLstBox.Name = "elementsLstBox";
            this.elementsLstBox.Size = new System.Drawing.Size(411, 316);
            this.elementsLstBox.TabIndex = 3;
            // 
            // itemLoader
            // 
            this.itemLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.itemLoader_DoWork);
            this.itemLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.itemLoader_RunWorkerCompleted);
            // 
            // LinearVisualizerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.elementsLstBox);
            this.Controls.Add(this.foundRchTxtBox);
            this.Controls.Add(this.searchongForLbl);
            this.Controls.Add(this.searchNumberTxtBox);
            this.Name = "LinearVisualizerControl";
            this.Size = new System.Drawing.Size(420, 447);
            this.Load += new System.EventHandler(this.LinearVisualizerControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UserControl1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label searchongForLbl;
        private System.Windows.Forms.TextBox searchNumberTxtBox;
        private System.Windows.Forms.RichTextBox foundRchTxtBox;
        private System.Windows.Forms.ListBox elementsLstBox;
        private System.ComponentModel.BackgroundWorker itemLoader;
    }
}
