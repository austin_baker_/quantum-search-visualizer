﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace AustinBaker_QuantumVisualizer
{
    public partial class visualizerFrm : Form
    {
        private List<long> data = new List<long>();
        Random rng = new Random();
        private bool ready = false;
        public visualizerFrm()
        {
            InitializeComponent();
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            if (mainTabCtrl.SelectedTab == linearTabPge)
            {
                if (!ready)
                {
                    if (randomizeBckWrk.IsBusy)
                        randomizeBckWrk.CancelAsync();
                    randomizeBckWrk.RunWorkerAsync();
                }
                soloLinVisCtrl.Clear();
                //Run linear search
                if (!soloLinearBckWrk.IsBusy)
                    soloLinearBckWrk.RunWorkerAsync();
            }
            else if (mainTabCtrl.SelectedTab == quantumTabPge)
            {
                //Run quantum search
            }
            else if (mainTabCtrl.SelectedTab == bothTabPge)
            {
                //Run both searches
            }
        }

        private void randomizeBtn_Click(object sender, EventArgs e)
        {
            if (mainTabCtrl.SelectedTab == linearTabPge)
            {
                if (randomizeBckWrk.IsBusy)
                    randomizeBckWrk.CancelAsync();
                randomizeBckWrk.RunWorkerAsync();
                soloLinVisCtrl.Clear();
            }
            else if (mainTabCtrl.SelectedTab == quantumTabPge)
            {
                //Run quantum search
            }
            else if (mainTabCtrl.SelectedTab == bothTabPge)
            {
                //Run both searches
            }
        }

        private void soloLinearBckWrk_DoWork(object sender, DoWorkEventArgs e)
        {
            int index = 0;
            bool found = false;
            int speed = 0;
            
            while (!found && index != -1)
            {
                Invoke((MethodInvoker)delegate { speed = (int)Math.Pow((double)(speedTrkBar.Maximum - speedTrkBar.Value), 2d); });
                Invoke((MethodInvoker)delegate
                {
                    try
                    {
                        found = soloLinVisCtrl.RunStep(Convert.ToInt64(searchForTxtBox.Text), ref index);
                    }
                    catch (Exception ex)
                    {
                    }
                });
                Thread.Sleep(speed);
                if (soloLinearBckWrk.CancellationPending)
                    break;
            }
        }

        private void soloQuantumBckWrk_DoWork(object sender, DoWorkEventArgs e)
        {
            bool found = false;
            int speed = 0;

            while (!found)
            {
                Invoke((MethodInvoker)delegate { speed = (int)Math.Pow((double)(speedTrkBar.Maximum - speedTrkBar.Value), 2d); });
                //Invoke((MethodInvoker)delegate { found = soloLinVisCtrl.RunStep(Convert.ToInt64(searchForTxtBox.Text), ref index); });
                Thread.Sleep(speed);
                if (soloLinearBckWrk.CancellationPending)
                    break;
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if (soloLinearBckWrk.IsBusy)
                soloLinearBckWrk.CancelAsync();
            if (soloQuantumBckWrk.IsBusy)
                soloQuantumBckWrk.CancelAsync();
            if (duoBckWrk.IsBusy)
                duoBckWrk.CancelAsync();
        }
        
        private void randomizeBckWrk_DoWork(object sender, DoWorkEventArgs e)
        {
            ready = false;
            Invoke((MethodInvoker)delegate {
                data.Clear();
                //Put all the numbers into data
                int number = 0;
                try{
                    number = Convert.ToInt32(dataSizeTxtBox.Text);
                }
                catch(Exception ex)
                {

                }
                for (int i = 0; i < number; i++)
                {
                    data.Add(i);
                    Thread.Sleep(0);
                    if (randomizeBckWrk.CancellationPending)
                        break;
                }
                //Then shuffle data with the Fisher-Yates algorithm
                for (int i = data.Count - 1; i >= 1; i--)
                {
                    int j = rng.Next(0, i);
                    long temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                    Thread.Sleep(0);
                    if (randomizeBckWrk.CancellationPending)
                        break;
                }
                ready = true;
            });
        }

        private void dataSizeTxtBox_TextChanged(object sender, EventArgs e)
        {
            
            if(randomizeBckWrk.IsBusy)
                randomizeBckWrk.CancelAsync();     
            randomizeBckWrk.RunWorkerAsync();
        }

        private void randomizeBckWrk_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            soloLinVisCtrl.Data = data;
        }
    }
}
