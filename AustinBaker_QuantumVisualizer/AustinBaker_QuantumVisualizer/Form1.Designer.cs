﻿namespace AustinBaker_QuantumVisualizer
{
    partial class visualizerFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(visualizerFrm));
            this.controlPnl = new System.Windows.Forms.Panel();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.randomizeBtn = new System.Windows.Forms.Button();
            this.searchForTxtBox = new System.Windows.Forms.TextBox();
            this.searchValueLbl = new System.Windows.Forms.Label();
            this.dataSizeTxtBox = new System.Windows.Forms.TextBox();
            this.dataSizeLbl = new System.Windows.Forms.Label();
            this.searchBtn = new System.Windows.Forms.Button();
            this.speedLbl = new System.Windows.Forms.Label();
            this.speedTrkBar = new System.Windows.Forms.TrackBar();
            this.mainTabCtrl = new System.Windows.Forms.TabControl();
            this.linearTabPge = new System.Windows.Forms.TabPage();
            this.soloLinVisCtrl = new LinearSearchVisualizer.LinearVisualizerControl();
            this.quantumTabPge = new System.Windows.Forms.TabPage();
            this.quantumPanel1 = new QuantumVisualizerPanel.quantumPanel();
            this.bothTabPge = new System.Windows.Forms.TabPage();
            this.soloLinearBckWrk = new System.ComponentModel.BackgroundWorker();
            this.soloQuantumBckWrk = new System.ComponentModel.BackgroundWorker();
            this.duoBckWrk = new System.ComponentModel.BackgroundWorker();
            this.randomizeBckWrk = new System.ComponentModel.BackgroundWorker();
            this.controlPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedTrkBar)).BeginInit();
            this.mainTabCtrl.SuspendLayout();
            this.linearTabPge.SuspendLayout();
            this.quantumTabPge.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlPnl
            // 
            this.controlPnl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.controlPnl.Controls.Add(this.cancelBtn);
            this.controlPnl.Controls.Add(this.randomizeBtn);
            this.controlPnl.Controls.Add(this.searchForTxtBox);
            this.controlPnl.Controls.Add(this.searchValueLbl);
            this.controlPnl.Controls.Add(this.dataSizeTxtBox);
            this.controlPnl.Controls.Add(this.dataSizeLbl);
            this.controlPnl.Controls.Add(this.searchBtn);
            this.controlPnl.Controls.Add(this.speedLbl);
            this.controlPnl.Controls.Add(this.speedTrkBar);
            this.controlPnl.Location = new System.Drawing.Point(0, 467);
            this.controlPnl.Name = "controlPnl";
            this.controlPnl.Size = new System.Drawing.Size(794, 112);
            this.controlPnl.TabIndex = 0;
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(378, 77);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 10;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // randomizeBtn
            // 
            this.randomizeBtn.Location = new System.Drawing.Point(193, 77);
            this.randomizeBtn.Name = "randomizeBtn";
            this.randomizeBtn.Size = new System.Drawing.Size(75, 23);
            this.randomizeBtn.TabIndex = 9;
            this.randomizeBtn.Text = "Randomize";
            this.randomizeBtn.UseVisualStyleBackColor = true;
            this.randomizeBtn.Click += new System.EventHandler(this.randomizeBtn_Click);
            // 
            // searchForTxtBox
            // 
            this.searchForTxtBox.Location = new System.Drawing.Point(410, 14);
            this.searchForTxtBox.Name = "searchForTxtBox";
            this.searchForTxtBox.Size = new System.Drawing.Size(100, 20);
            this.searchForTxtBox.TabIndex = 8;
            // 
            // searchValueLbl
            // 
            this.searchValueLbl.AutoSize = true;
            this.searchValueLbl.Location = new System.Drawing.Point(345, 17);
            this.searchValueLbl.Name = "searchValueLbl";
            this.searchValueLbl.Size = new System.Drawing.Size(59, 13);
            this.searchValueLbl.TabIndex = 7;
            this.searchValueLbl.Text = "Search For";
            // 
            // dataSizeTxtBox
            // 
            this.dataSizeTxtBox.Location = new System.Drawing.Point(210, 14);
            this.dataSizeTxtBox.MaxLength = 7;
            this.dataSizeTxtBox.Name = "dataSizeTxtBox";
            this.dataSizeTxtBox.Size = new System.Drawing.Size(100, 20);
            this.dataSizeTxtBox.TabIndex = 6;
            this.dataSizeTxtBox.TextChanged += new System.EventHandler(this.dataSizeTxtBox_TextChanged);
            // 
            // dataSizeLbl
            // 
            this.dataSizeLbl.AutoSize = true;
            this.dataSizeLbl.Location = new System.Drawing.Point(132, 17);
            this.dataSizeLbl.Name = "dataSizeLbl";
            this.dataSizeLbl.Size = new System.Drawing.Size(72, 13);
            this.dataSizeLbl.TabIndex = 5;
            this.dataSizeLbl.Text = "Data Set Size";
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(15, 77);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 2;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // speedLbl
            // 
            this.speedLbl.AutoSize = true;
            this.speedLbl.Location = new System.Drawing.Point(12, 35);
            this.speedLbl.Name = "speedLbl";
            this.speedLbl.Size = new System.Drawing.Size(87, 13);
            this.speedLbl.TabIndex = 1;
            this.speedLbl.Text = "Animation Speed";
            // 
            // speedTrkBar
            // 
            this.speedTrkBar.Location = new System.Drawing.Point(3, 3);
            this.speedTrkBar.Name = "speedTrkBar";
            this.speedTrkBar.Size = new System.Drawing.Size(104, 45);
            this.speedTrkBar.TabIndex = 0;
            // 
            // mainTabCtrl
            // 
            this.mainTabCtrl.Controls.Add(this.linearTabPge);
            this.mainTabCtrl.Controls.Add(this.quantumTabPge);
            this.mainTabCtrl.Controls.Add(this.bothTabPge);
            this.mainTabCtrl.ItemSize = new System.Drawing.Size(58, 18);
            this.mainTabCtrl.Location = new System.Drawing.Point(0, 0);
            this.mainTabCtrl.Name = "mainTabCtrl";
            this.mainTabCtrl.SelectedIndex = 0;
            this.mainTabCtrl.Size = new System.Drawing.Size(794, 461);
            this.mainTabCtrl.TabIndex = 1;
            // 
            // linearTabPge
            // 
            this.linearTabPge.Controls.Add(this.soloLinVisCtrl);
            this.linearTabPge.Location = new System.Drawing.Point(4, 22);
            this.linearTabPge.Name = "linearTabPge";
            this.linearTabPge.Padding = new System.Windows.Forms.Padding(3);
            this.linearTabPge.Size = new System.Drawing.Size(786, 435);
            this.linearTabPge.TabIndex = 0;
            this.linearTabPge.Text = "Linear";
            this.linearTabPge.UseVisualStyleBackColor = true;
            // 
            // soloLinVisCtrl
            // 
            this.soloLinVisCtrl.Data = ((System.Collections.Generic.List<long>)(resources.GetObject("soloLinVisCtrl.Data")));
            this.soloLinVisCtrl.Elements = 100;
            this.soloLinVisCtrl.Location = new System.Drawing.Point(-1, 0);
            this.soloLinVisCtrl.Name = "soloLinVisCtrl";
            this.soloLinVisCtrl.Size = new System.Drawing.Size(420, 435);
            this.soloLinVisCtrl.TabIndex = 0;
            // 
            // quantumTabPge
            // 
            this.quantumTabPge.AutoScroll = true;
            this.quantumTabPge.Controls.Add(this.quantumPanel1);
            this.quantumTabPge.Location = new System.Drawing.Point(4, 22);
            this.quantumTabPge.Name = "quantumTabPge";
            this.quantumTabPge.Padding = new System.Windows.Forms.Padding(3);
            this.quantumTabPge.Size = new System.Drawing.Size(786, 435);
            this.quantumTabPge.TabIndex = 1;
            this.quantumTabPge.Text = "Quantum";
            this.quantumTabPge.UseVisualStyleBackColor = true;
            // 
            // quantumPanel1
            // 
            this.quantumPanel1.AutoScroll = true;
            this.quantumPanel1.Data = ((System.Collections.Generic.List<long>)(resources.GetObject("quantumPanel1.Data")));
            this.quantumPanel1.Location = new System.Drawing.Point(3, 6);
            this.quantumPanel1.Name = "quantumPanel1";
            this.quantumPanel1.ScaleUnitSize = 100;
            this.quantumPanel1.Size = new System.Drawing.Size(658, 423);
            this.quantumPanel1.TabIndex = 0;
            // 
            // bothTabPge
            // 
            this.bothTabPge.Location = new System.Drawing.Point(4, 22);
            this.bothTabPge.Name = "bothTabPge";
            this.bothTabPge.Size = new System.Drawing.Size(786, 435);
            this.bothTabPge.TabIndex = 2;
            this.bothTabPge.Text = "Both";
            this.bothTabPge.UseVisualStyleBackColor = true;
            // 
            // soloLinearBckWrk
            // 
            this.soloLinearBckWrk.WorkerReportsProgress = true;
            this.soloLinearBckWrk.WorkerSupportsCancellation = true;
            this.soloLinearBckWrk.DoWork += new System.ComponentModel.DoWorkEventHandler(this.soloLinearBckWrk_DoWork);
            // 
            // randomizeBckWrk
            // 
            this.randomizeBckWrk.WorkerSupportsCancellation = true;
            this.randomizeBckWrk.DoWork += new System.ComponentModel.DoWorkEventHandler(this.randomizeBckWrk_DoWork);
            this.randomizeBckWrk.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.randomizeBckWrk_RunWorkerCompleted);
            // 
            // visualizerFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 579);
            this.Controls.Add(this.mainTabCtrl);
            this.Controls.Add(this.controlPnl);
            this.Name = "visualizerFrm";
            this.Text = "Visualizer";
            this.controlPnl.ResumeLayout(false);
            this.controlPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.speedTrkBar)).EndInit();
            this.mainTabCtrl.ResumeLayout(false);
            this.linearTabPge.ResumeLayout(false);
            this.quantumTabPge.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel controlPnl;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label speedLbl;
        private System.Windows.Forms.TrackBar speedTrkBar;
        private System.Windows.Forms.TabControl mainTabCtrl;
        private System.Windows.Forms.TabPage linearTabPge;
        private System.Windows.Forms.TabPage quantumTabPge;
        private System.Windows.Forms.TabPage bothTabPge;
        private System.Windows.Forms.Button randomizeBtn;
        private System.Windows.Forms.TextBox searchForTxtBox;
        private System.Windows.Forms.Label searchValueLbl;
        private System.Windows.Forms.TextBox dataSizeTxtBox;
        private System.Windows.Forms.Label dataSizeLbl;
        private System.Windows.Forms.Button cancelBtn;
        private LinearSearchVisualizer.LinearVisualizerControl soloLinVisCtrl;
        private System.ComponentModel.BackgroundWorker soloLinearBckWrk;
        private System.ComponentModel.BackgroundWorker soloQuantumBckWrk;
        private System.ComponentModel.BackgroundWorker duoBckWrk;
        private System.ComponentModel.BackgroundWorker randomizeBckWrk;
        private QuantumVisualizerPanel.quantumPanel quantumPanel1;
    }
}

